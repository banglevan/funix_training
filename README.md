# funix_training
## week 6 2021-05-30 to 2021-05-06

**namtp**
- Tìm hiểu cách thiết lập anchor box cho training
- Tìm hiểu các chỉ số phân tích dữ liệu ảnh, dùng cho huấn luyện

## week 4 2021-05-15 to 2021-05-22

**tritc**
- Tìm hiểu và thực hành các tính năng của
    - multiprocess, multithread
    - zmq, (hoặc socket sử dụng các options cho streaming, e.g. cơ chế bắt tay, protocol...)
    - Thực hành tách luồng cho client, tách luồng gửi và nhận đối với mỗi client
- Báo cáo về các mục trên.

**namtp**
- Tìm hiểu về crawler, các công cụ gắn nhãn dữ liệu, mục tiêu 500 sample positive đối với bài toán smoke detection
- Tìm hiểu về các metric đánh giá chất lượng các detector (mAP, IoU...)
- Tìm hiểu các format annotations hiện có cho detector (COCO, VOC...)
- Tìm hiểu cách thiết lập anchor box cho training
- Tìm hiểu các chỉ số phân tích dữ liệu ảnh, dùng cho huấn luyện
    * các mục tiêu bài tập này kéo dài hai tuần

## week 3 2021-05-08 to 2021-05-15
**tritc**
- Implement a zmq project: (1) setup multi clients (multithread); (2) send video frame objects; (3) multi thread for server include receiving thread and display thread; (4) display the video frame on the server.
- Documentations: focus for the bottlenecks

**namtp**
- Overview (short report) for object detection network
- Overview for smoke detection network
- Complete a simple project (smoke detection), include: (1) overview and benchmarking; (2) data preparing and analysis; (3) Implement network; (4) training, evaluate the performance and propose the improvement solution.

## week 2 2021-04-24 to 2021-05-02
**tritc**
- Overview for structure of AI server (AI Computer vision), which use Zmq, protobuf, threading tools
- Implement a simple network, define the communication flows for clients-servers, MongoDB for database
- Splitting process (thread) for each client
```
- (1) documentations
- (2) program
```
**namtp**
- Replenish unfinished task

## week 1 2021-04-17 to 2021-04-24
**tritc**
- Overview for web backend: introduction, applications, constitution model
- Overview for working flows: models (e.g. MVC, MTV...), the relation between client(s) and server(s)
- Overview for database: SQL, NoSQL
- Overview for tools (Python programming): Socket, protobuf, zmq, flask, Django, multithread, multiprocess
- A little view for web front end: html, js
- Implement a simple Flask project
- *Output*: 
```
(1): Report
(2): Simple project, documentations
```
    
**namtp**
- Overview for (fire) smoke detection models
- Overview for few shot detection technology
- The contents structure are following:
```
- (1) Reference title
- (2) Purpose
- (3) Methodology
- (4) Data
- (5) Effective
```
- *Output*:
```
- (1) Valid data link (set)
- (2) Models **benchmarking**
- (3) Report
```
