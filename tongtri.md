# tongtri personal notes

## week_1

**Notes:** [link](https://www.notion.so/AI-project-_-TongTri-edfeff2ce1a845d0a142cfaa08d1da3b)

## week_2
**To do:**
- Overview of multithread, multiprocess, python threading
- How to create AI server by Python?
- Overview of *ZMQ multiclient multithread*

**Output:**
```
(1): design an AI server by Python
(2): create 1 client and connect to server
```
