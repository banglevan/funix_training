import socket
import time
from imutils.video import VideoStream
import imagezmq
import cv2
from pip._vendor.distlib.compat import raw_input

input = raw_input('Enter video path: ')
sender = imagezmq.ImageSender(connect_to='tcp://localhost:5555')

rpi_name = input  # send RPi hostname with each image

vid = VideoStream(src=input).start()
# vid = cv2.VideoCapture(input)

time.sleep(2.0)  # allow camera sensor to warm up
while True:  # send images as stream until Ctrl-C
    image = vid.read()
    sender.send_image(rpi_name, image)